# My React Template 2020

## Requirements
- git
- nodeJS
- yarn

## Getting Started

### Clone this Repository
  ```
  $ git clone https://bitbucket.org/lee_fergusson/react-template/src/master/ your_project
  ```

### Install Packages
```
$ yarn install
```

### Start the development server
```
$ yarn start
```

### Make something Cool
