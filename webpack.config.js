// =============================================================================
//  Filename: webpack.config.js (Development)
// -----------------------------------------------------------------------------

// ===================================================================
// Module Imports
// -------------------------------------------------------------------
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  // =======================================================
  // General Settings.
  // -------------------------------------------------------
  mode: 'development',
  entry: './src/index.jsx',
  devtool: 'eval-source-map',
  // =======================================================
  // Output Settings.
  // -------------------------------------------------------
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  // =======================================================
  // Webpack Plugins.
  // -------------------------------------------------------
  plugins: [
    new HtmlWebpackPlugin(
      { template: path.resolve(__dirname, 'src/index.html') },
    )],
  // =======================================================
  // Module Settings.
  // -------------------------------------------------------
  module: {
    // ===========================================
    // Loader Rules.
    // -------------------------------------------
    rules: [
      // Javascript Loader.
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader',
      },
      // Style Loaders.
      {
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  // =======================================================
  // Resolver Settings.
  // -------------------------------------------------------
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  // =======================================================
  // Development Server Settings.
  // -------------------------------------------------------
  devServer: {
    historyApiFallback: true,
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000,
  },
};
