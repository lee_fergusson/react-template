import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

import './styles/normalize.css';
import './styles/Application.scss';
import HomePage from './views/HomePage';
import AboutPage from './views/AboutPage';

const Application = () => (
  <>
    {/* Header goes here */}
    <main>
      <Router>
        <Switch>
          <Route path="/about">
            <AboutPage />
          </Route>
          <Route path="/">
            <HomePage />
          </Route>
        </Switch>
      </Router>
    </main>
    {/* Footer goes here */}
  </>
);

export default Application;
