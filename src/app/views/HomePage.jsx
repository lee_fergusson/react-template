import React from 'react';
import { Link } from 'react-router-dom';

const HomePage = () => (
  <div className="hero">
    <div className="container">
      <h2>Welcome To My React Template</h2>
      <Link to="/about">About</Link>
    </div>
  </div>
);

export default HomePage;
