import React from 'react';

const AboutPage = () => (
  <>
    <div className="hero">
      <div className="container">
        <h2>About</h2>
        <p>
          This is a simple project skeleton for creating React Applications.
        </p>
        <p>
          It comes with a suite of development tools including ESLint, Babel transpiler,
          and WebPack. With a testing setup with Mocha, Chai and NYC
        </p>
      </div>
    </div>
  </>
);

export default AboutPage;
