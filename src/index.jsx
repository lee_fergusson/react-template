// =============================================================================
//  Filename: index.jsx
// -----------------------------------------------------------------------------

// ===================================================================
//  Module Imports
// -------------------------------------------------------------------
import React from 'react';
import ReactDOM from 'react-dom';

// ===================================================================
//  Project Imports
// -------------------------------------------------------------------
import Application from './app/Application';

// Bind the application to the root element.
ReactDOM.render(<Application />, document.getElementById('root'));
